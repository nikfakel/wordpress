<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package seo18
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php /*the_title( '<h1 class="entry-title">', '</h1>' ); */?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
		$c=get_the_content();
		 if ($c!='') {
		 the_content();
		}
		else {echo "<p>Страница находится на стадии разработки</p>";}?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php edit_post_link( __( 'Edit', 'seo18' ), '<span class="edit-link">', '</span>' ); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
